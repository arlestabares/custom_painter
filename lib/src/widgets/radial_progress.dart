import 'dart:math';

import 'package:flutter/material.dart';

class RadialProgress extends StatefulWidget {
  final porcentaje;
  final Color colorPrimario;
  final Color colorSecundario;
  final grosorPrimario;
  final grosorSecundario;

  const RadialProgress(
      {@required this.porcentaje,
      this.colorPrimario = Colors.green,
      this.colorSecundario = Colors.purple,
      this.grosorPrimario = 4.0,
      this.grosorSecundario = 10.0});

  @override
  _RadialProgressState createState() => _RadialProgressState();
}

class _RadialProgressState extends State<RadialProgress>
    with SingleTickerProviderStateMixin {
  late AnimationController
      controller; //Cada que definimos un controlador, debemos
//sobreescribir los metodos de initState(), y setState().
  double? porcentajeAnterior;

  @override
  void initState() {
    porcentajeAnterior = widget.porcentaje;
    controller = new AnimationController(
        vsync: this, duration: Duration(milliseconds: 20));
    super.initState();
  }

  @override
  void setState(fn) {
    super.setState(fn);
  }

  @override
  Widget build(BuildContext context) {
    controller.forward(from: 0.0);
    final diferenciaAnimar = widget.porcentaje - porcentajeAnterior;
    porcentajeAnterior = widget.porcentaje;

    return AnimatedBuilder(
      animation: controller,
      builder: (context, child) {
        return Container(
          //child: Text('${widget.porcentaje}'),
          padding: EdgeInsets.all(10),
          width: double.infinity,
          height: double.infinity,
          child: CustomPaint(
            //el valor del controller.value , esta entre 0 y 1
            painter: _MiRadialProgress(
              (widget.porcentaje - diferenciaAnimar) +
                  (diferenciaAnimar * controller.value),
              widget.colorPrimario,
              widget.colorSecundario,
              widget.grosorPrimario,
              widget.grosorSecundario,
            ),
          ),
        );
      },
    );
  }
}

class _MiRadialProgress extends CustomPainter {
  final porcentaje;
  final Color colorPrimario;
  final Color colorSecundario;
  final double grosorPrimario;
  final double grosorSecundario;

  _MiRadialProgress(
    this.porcentaje,
    this.colorPrimario,
    this.colorSecundario,
    this.grosorPrimario,
    this.grosorSecundario,
  );

  @override
  void paint(Canvas canvas, Size size) {
    final Rect rect = Rect.fromCircle(center: Offset(0, 0), radius: 180);

    final Gradient gradiente = new LinearGradient(
        colors: <Color>[Color(0xffC012FF), Color(0xff6D05E8), Colors.red]);

    //Circulo Completado
    final paint = new Paint() //Creamos el Lapiz y definimos unas carateristicas
      ..strokeWidth = grosorPrimario //ancho del circulo
      ..color = colorPrimario //color del lapiz
      ..style = PaintingStyle.stroke; //estilo del lapiz

    Offset center = new Offset(size.width * 0.5, size.height / 2);
    double radio = min(size.width * 0.5, size.height * 0.5);

    //Ahora le digo a  Flutter cómo quiero que se dibuje el circulo y el centro
    canvas.drawCircle(center, radio, paint);

    //Creacion del ARCO O curva

    final paintArco = new Paint()
      ..strokeWidth = grosorSecundario //grosor de la linea circular
      ..strokeCap = StrokeCap.round
      // ..color = colorSecundario
      ..shader = gradiente.createShader(rect)
      ..style = PaintingStyle.stroke;

    //Parte que se debera ir llenando del arco definido
    double arcAngle = 2 * pi * (porcentaje / 100);
    //dibujamos el arco u angulo con drawArc()
    canvas.drawArc(
        //Rect es como el espacio donde deseamos poner o ubicar el angulo
        Rect.fromCircle(center: center, radius: radio),
        -pi / 2,
        arcAngle,
        false,
        paintArco);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => true;
}
