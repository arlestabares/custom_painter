//import 'package:custom_painter/src/pages/animaciones_page.dart';
import 'package:custom_painter/src/pages/graficas_circulares_page.dart';
import 'package:flutter/material.dart';
//import 'package:custom_painter/src/labs/circularProgress_page.dart';
//import 'package:custom_painter/src/retos/cuadrado_animado.dart';
//import 'package:custom_painter/src/pages/animaciones_page.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Diseños App',
      home: GraficasCircularesPage(),
    );
  }
}
